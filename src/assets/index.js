import airbnb from "./airbnb.png";
import loupe from "./loupe.png"
import bill from "./bill.png";
import binance from "./binance.png";
import card from "./card.png";
import manager from './manager.png'
import coinbase from "./coinbase.png";
import dropbox from "./dropbox.png";
import logo from "./logo.svg";
import quotes from "./quotes.svg";
import robot from "./robot.png";
import send from "./Send.svg";
import shield from "./Shield.svg";
import star from "./Star.svg";
import menu from "./menu.svg";
import close from "./close.svg";
import google from "./google.svg";
import apple from "./apple.svg";
import arrowUp from "./arrow-up.svg";
import discount from "./Discount.svg";
import facebook from "./facebook.svg";
import instagram from "./instagram.svg";
import linkedin from "./linkedin.svg";
import twitter from "./twitter.svg";
import people01 from "./people01.png";
import people02 from "./people02.png";
import people03 from "./people03.png";
import web from "./web.png"
import report from "./report.png"
import cow from "./cow.png"
import builder from "./builder.png"
import shop from "./shop.png"
import tools from "./tools.png"
import attention from "./attention.png"
import envoyer from "./envoyer.png"
import arbre from "./arbre.png"
import women from "./women.jpg"
import monimo from "./monimo.png"
import bg_left from "./bg_left.png"
import about from "./about.png"
export {
  about,
  bg_left,
  monimo,
  women,
  airbnb,
  arbre,
  builder,
  shop,
  cow,
  report,
  web,
  tools,
  loupe,
  attention,
  envoyer,
  bill,
  binance,
  card,
  manager,
  coinbase,
  dropbox,
  logo,
  quotes,
  robot,
  send,
  shield,
  star,
  menu,
  close,
  google,
  apple,
  arrowUp,
  discount,
  facebook,
  instagram,
  linkedin,
  twitter,
  people01,
  people02,
  people03,
};
