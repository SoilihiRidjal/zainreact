import { people01, people02, people03, facebook, instagram, linkedin, twitter, airbnb, binance, coinbase, dropbox, send, shield, star, web, builder, tools, report, shop, cow  } from "../assets";
import { IoIosList } from "react-icons/io";
export const navLinks = [
  {
    id: "home",
    title: "Home",
  },
  {
    id: "features",
    title: "Features",
  },
  {
    id: "product",
    title: "Product",
  },
  {
    id: "clients",
    title: "Clients",
  },
];

export const Tinder = [
  {
    id: "1",
    title : "Conception du site de Comorlab",
    expiration : "29/10/2023",
    publication : "il y a 2 semaine",
    ville : "Moroni",
    categories : "Develloppement web",
    type : "Appel à candidature",
    reponse : "1",
    views : "0"
  },
  {
    id: "2",
    title : "Renovation de la banque central",
    expiration : "29/10/2023",
    publication : "il y a 2 une minute",
    ville : "Moroni",
    categories : "BTP",
    type : "Appel à duré limité",
    reponse : "0",
    views : "30"
  },
  {
    id: "3",
    title : "Achat en gros de tomate bio",
    expiration : "29/10/2023",
    publication : "il y a 4 semaine",
    ville : "Moroni",
    categories : "Commerce",
    type : "Appel commerçial",
    reponse : "40",
    views : "40"
  },
  {
    id: "4",
    title : "Mise en reseaux de la Meck",
    expiration : "29/10/2023",
    publication : "il y a 20s",
    ville : "Moroni",
    categories : "Informatique",
    type : "Appel d'offre ouvert",
    reponse : "90",
    views : "180"
  },
];

export const features = [
  {
    id: "feature-1",
    icon: star,
    title: "Note de la clientel",
    content:
      "Connectez-vous en tant que clients ou utilisateur et soyez  le pértinents sur les nouvelle oppurtinité",
  },
  {
    id: "feature-2",
    icon: shield,
    title: "100% Sécuriser",
    content:
      "Nous prenons des mesures proactives pour nous assurer que vos informations et la navigation sur notre site soient sécuritaires .",
  },
  {
    id: "feature-3",
    icon: send,
    title: "Partage-Partage",
    content:
      " Chaque utilisateur a un reseaux de partage Chaque utilisateur a un reseaux de partage Chaque utilisateur a un reseaux de partage  ",
  },
];

export const feedback = [
  {
    id: "feedback-1",
    content:
      "Money is only a tool. It will take you wherever you wish, but it will not replace you as the driver.",
    name: "Herman Jensen",
    title: "Founder & Leader",
    img: people01,
  },
  {
    id: "feedback-2",
    content:
      "Money makes your life easier. If you're lucky to have it, you're lucky.",
    name: "Steve Mark",
    title: "Founder & Leader",
    img: people02,
  },
  {
    id: "feedback-3",
    content:
      "It is usually people in the money business, finance, and international trade that are really rich.",
    name: "Kenn Gallagher",
    title: "Founder & Leader",
    img: people03,
  },
];

export const categories = [
  {
    id: "cat-1",
    title : "Web",
    icon : web
  },
  {
    id: "cat-2",
    title : "BTP",
    icon : builder
  },
  {
    id: "cat-3",
    title : "Design",
    icon : tools
  },
  {
    id: "cat-4",
    title : "Analitics",
    icon : report
  },
  {
    id: "cat-5",
    title : "Commercial",
    icon : shop
  },
  {
    id: "cat-6",
    title : "Agriculture",
    icon : cow
  }
  
]

export const howItWork = [
  {
    id : 'works-1',
    steps : "1",
    tutorial : " Connectez - vous ou créer un compte Ainsi vous pouvez publier ou consulter des offre "
  },
  {
    id : 'works-4',
    steps : "4",
    tutorial : " Lors de la publication de votre offre vérifier soigneusement  si vous avez bien respecter les terme du formulaire de publication "
  },
  {
    id : 'works-2',
    steps : "2",
    tutorial : " Pour publier une offre Cliquer sur nouvelle offre "
  },
 
  
  
  {
    id : 'works-5',
    steps : "5",
    tutorial : " votre offre apparaitra sur le site suivant la catégorie dont elle appartient "
  },
  {
    id : 'works-3',
    steps : "3",
    tutorial : " Choisissez un package Cette option est disponible dès la création d'un nouveau compte "
  },
]

export const stats = [
  {
    id: "stats-1",
    title: "User Active",
    value: "3800+",
  },
  {
    id: "stats-2",
    title: "Trusted by Company",
    value: "230+",
  },
  {
    id: "stats-3",
    title: "Transaction",
    value: "$230M+",
  },
];


export const footerLinks = [
  {
    title: "Useful Links",
    links: [
      {
        name: "Content",
        link: "https://www.hoobank.com/content/",
      },
      {
        name: "How it Works",
        link: "https://www.hoobank.com/how-it-works/",
      },
      {
        name: "Create",
        link: "https://www.hoobank.com/create/",
      },
      {
        name: "Explore",
        link: "https://www.hoobank.com/explore/",
      },
      {
        name: "Terms & Services",
        link: "https://www.hoobank.com/terms-and-services/",
      },
    ],
  },
  {
    title: "Community",
    links: [
      {
        name: "Help Center",
        link: "https://www.hoobank.com/help-center/",
      },
      {
        name: "Partners",
        link: "https://www.hoobank.com/partners/",
      },
      {
        name: "Suggestions",
        link: "https://www.hoobank.com/suggestions/",
      },
      {
        name: "Blog",
        link: "https://www.hoobank.com/blog/",
      },
      {
        name: "Newsletters",
        link: "https://www.hoobank.com/newsletters/",
      },
    ],
  },
  {
    title: "Partner",
    links: [
      {
        name: "Our Partner",
        link: "https://www.hoobank.com/our-partner/",
      },
      {
        name: "Become a Partner",
        link: "https://www.hoobank.com/become-a-partner/",
      },
    ],
  },
];

export const socialMedia = [
  {
    id: "social-media-1",
    icon: instagram,
    link: "https://www.instagram.com/",
  },
  {
    id: "social-media-2",  
    icon: facebook,
    link: "https://www.facebook.com/",
  },
  {
    id: "social-media-3",
    icon: twitter,
    link: "https://www.twitter.com/",
  },
  {
    id: "social-media-4",
    icon: linkedin,
    link: "https://www.linkedin.com/",
  },
];

export const clients = [
  {
    id: "client-1",
    logo: airbnb,
  },
  {
    id: "client-2",
    logo: binance,
  },
  {
    id: "client-3",
    logo: coinbase,
  },
  {
    id: "client-4",
    logo: dropbox,
  },
];

export const LinkMenue = [
  {
    title : "Acceuil",
    id : "home"
  },
  {
    title : "Qui sommes nous ?",
    id : "about"
  },
  {
    title : "Services",
    id : "service"
  },
  {
    title : "Tarifs",
    id : "price"
  },
  {
    title : "Contact",
    id : "contact"
  },
];

export const cat = [
  {
    id: "1",
    title : "Web design",
    classe : "bg-gradient-to-r from-[#F647C5] to-[#6272FF] ",
    description : "Web marketing, mockup, ads,bannier ..."
  },
  {
    id: "2",
    title : "App mobile",
    classe : "bg-gradient-to-r from-[#F14610] to-[#005A4A] ",
    description : "Web marketing, mockup, ads,bannier ..."
  },
  {
    id: "3",
    title : "App mobile",
    classe : "bg-gradient-to-r from-[#F14610] to-[#005A4A] ",
    description : "Web marketing, mockup, ads,bannier ..."
  },
  {
    id: "4",
    title : "App mobile",
    classe : "bg-gradient-to-r from-[#F14610] to-[#005A4A] ",
    description : "Web marketing, mockup, ads,bannier ..."
  },
  {
    id: "5",
    title : "App mobile",
    classe : "bg-gradient-to-r from-[#F14610] to-[#005A4A] ",
    description : "Web marketing, mockup, ads,bannier ..."
  },
  {
    id: "6",
    title : "App mobile",
    classe : "bg-gradient-to-r from-[#F14610] to-[#005A4A] ",
    description : "Web marketing, mockup, ads,bannier ..."
  },
]
export const Price = [
  {
    id : "p1",
    title : 'Gratuit',
    price : "0KMF",
    inscription : "gratuit",
    consultation : "30",
    publication : "aucune",
    postulation : "Non autoriser",
    publicite : "Aucune restriction",
    alert : "Non autoriser",
    button_gradient : "bg-gradient-to-r from-[#FF0F00] to-[#FF048B] "

  },
  {
    id : "p2",
    title : "Premium",
    price : "125 000KMF",
    inscription : "gratuit",
    consultation : "Illimité",
    publication : "20",
    publicite : "Facultatif",
    Postulation : "30 autoriser",
    alert : "Non autoriser",
    button_gradient : "bg-gradient-to-r from-[#4200FF] to-[#DC04FF] "

  },
  {
    id : "p3",
    title : "Busness",
    price : "350 000KMF",
    inscription : "gratuit",
    consultation : "Illimité",
    publication : "Facultatif",
    publicite : "Facultatif",
    Postulation : "illimité",
    alert : "Création d'alert",
    button_gradient : "bg-gradient-to-r from-[#00B2FF] to-[#19FF22] " 

  },
]

export const icon = [

  {
  
    gradient : "bg-gradient-to-r from-[#45FF26] via-[#2207FF] ",
  },
  {
    gradient : "bg-gradient-to-r from-[#E80000] via-[#0047FF] ",
  },
  {
    gradient : "bg-gradient-to-r from-[#08BF5C] via-[#0DB6FF] ",
  },
  {
    gradient : "bg-gradient-to-r from-[#FB5F5F] via-[#FAB70D] ",
  },
  {
    gradient : "bg-gradient-to-r from-[#FFA5A5] via-[#C80131] ",
  },
]