import React from 'react'
import styles, { layout } from '../style'
import Button from './Button'
const CTA = () => (
  <section className={`${styles.flexCenter} ${styles.padding} ${styles.marginY}  sm:flex-row flex-col bg-black-gradient box-shadow rounded-[20px]`} >
      <div>
          <h2 className={styles.heading2}>Lorem ipsum dolor sit amet.</h2>
          <p className={`${styles.paragraph} max-w-[480px] mt-5 `} >Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nesciunt, ipsa!</p>
      </div>
      <div className={`${styles.flexCenter} sm:ml-10 ml-0 sm:mt-0 mt-10`} >
        <Button/>
      </div>
  </section>
)

export default CTA