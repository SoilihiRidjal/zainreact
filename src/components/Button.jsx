import React from 'react'

const Button = ({styles}) => (
  <button type='button' className={`py-4 px-6 bg-transparent border-[3px] border-rose  font-poppins font-medium text-[18px] text-white
  outline-none rounded-[10px] ${styles} ` } >Commencer</button>
)

export default Button