import Navbar from "./Navbar";
import Billing from "./Billing";
import CardDeal from "./CardDeal";
import Business from "./Business";
import Clients from "./Clients";
import CTA from "./CTA";
import Stats from "./Stats";
import Footer from "./Footer";
import Testimonials from "./Testimonials";
import Hero from "./Hero";
import SearchForm from "./SearchForm";
import Header from "./Header"
import NewTinder from "./NewTinder";
import TinderCategories from "./TinderCategories";
import Login from "./userControll/Login";
import Home from "./home/home";
import SectionHeader from "./home/SectionHeader";
import Categories from "./home/Categories";
import About from "./home/About";
import Forfait from "./home/Forfait";
import FooterHome from "./home/FooterHome";
import UserPage from "./userControll/userpage/UserPage";
export {

  Login,
  UserPage,
  Home,
  SectionHeader,
  Categories,
  About,
  Forfait,
  FooterHome,
  Navbar,
  Header,
  Billing,
  CardDeal,
  Business,
  Clients,
  CTA,
  Stats,
  Footer,
  Testimonials,
  Hero,
  SearchForm,
  NewTinder,
  TinderCategories
};