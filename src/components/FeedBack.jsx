import React from 'react'
import { quotes } from '../assets'
import styles from '../style'
const FeedBack = ({content,name,title,img}) => (
  <div className='flex flex-col px-10 py-12 rounded-[20px] 
  justify-between max-w-[370px] md:mr-10 sm:mr-5 mr-0 my-5 feedback-card ' >
      <img src={quotes} alt="cote" className='w-[42px] h-[27px] object-contain ' />
      <p className='font-poppins font-normal text-[18px] leading-[32px] hover:text-white text-dimWhite my-10 ' > {content} </p>
      <div className='flex flex-row' >
          <img src={img} alt="img_users_testamonial" className=" w-[48px] h-[48px] rounded-full "  />
          <div className='flex flex-col ml-4'>
                <h4 className='font-poppins font-semibold text-white sm:text-[20px] text[10px]' > {name} </h4>
                <p className={`${styles.paragraph}text-white text-[16px] font-normal`}> {title} </p>
          </div>
      </div>
  </div>
)
export default FeedBack