import React from 'react'
import { features } from '../constants'
import styles, {layout} from '../style'
import Button from './Button'


const FeatureCard = ({title,index,content,icon})=>(
    <div className={`flex flex-row p-6 rounded-[20px] ${index !==features.length ? 'mb-6' : 'mb-0'} hover:bg-secondary `}>
      <div className={`w-[64px] h-[64px] bg-secondary rounded-full ${styles.flexCenter}`} >
        <img src={icon} alt="icon" className="w-[50%] h-[50%] object-contain" />
      </div>
      <div className='flex-1 flex flex-col ml-3'>
            <h4 className={`font-poppins font-semibold text-white text-[18px] leading-[20px] mb-1 `}>{title}</h4>
            <p className={`${styles.paragraph} font-normal leading-[24px] max-w-[400] `}> {content} </p>
      </div>
    </div>
)


const Business = () => (
  <section id='features' className={layout.section} >
    <div className={layout.sectionInfo} >
        <h2 className={styles.heading2}>Les bases  <br className='sm:block hidden'/> de notre business.</h2>
        <p className={`${styles.paragraph} max-w-[480px] mt-5`}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci minus sit totam nobis ipsum non? Eum nostrum, at, blanditiis praesentium a in ea voluptate dicta vitae enim asperiores, ut tenetur.</p>
        <Button styles = "mt-10" />
    </div>
    <div className={`${layout.sectionImg} flex-col`} >
      
        {features.map((feature,index)=>(
          <FeatureCard key={feature.id} {...feature} index = {index}/>
        ))}

    </div>
  </section>
)

export default Business
