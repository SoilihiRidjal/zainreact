import React from 'react'
import { shop } from '../assets'
import { categories } from '../constants'
import styles, { layout } from '../style'

const TinderCategories = () => {
  return (
     <section className={`${styles.paddingY} ${styles.marginX} `} >
      <h4 className='font-poppins font-semibold text-white text-[20px] sm:text-[30px] ' >Catégories d'offre</h4>
      <div className='items-center justify-center grid  grid-rows-1 grid-flow-dense  md:grid-rows-2 sm:grid-flow-col gap-10 mt-10  sm:grid-rows-3 ' >
      {
          categories.map((cat)=>(
            <div key={cat.id} className='' >
            <div className=' flex items-center gap-8 w-[300px] h-[200px] bg-secondary rounded-[20px] justify-center ' >
                <img src={cat.icon} alt="" className=' h-[100px] w-[100px] '  />
                <h6 className=' font-poppins font-normal text-[15px] text-white' >{cat.title}</h6>
            </div>
      </div>
          ))
      }
      </div>
     
       
     </section>
  )
}

export default TinderCategories
