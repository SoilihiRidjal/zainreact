import React from 'react'
import { manager } from '../assets'
import styles, { layout } from '../style'
import Button from './Button'

const Header = () => {
  return (
    <section className={`${layout.sectionReverse}  md:flex flex-row `}  >
       
            <div className=' mt-0 md:mt-[-85px] '>
                <img src={manager} alt="manager" className='  w-[530px] h-[390px] sm:h-[530px]  md:h-[530px] ' />
                <div className='   w-[630px] sm:w-full md:w-[700px]   h-[7px] bg-rose ' ></div>
            </div>
            <div className=' justify-start w-[590px] sm:mt-[-40px] xs:mt-[20px] ' >
                <div className='md:w-[475px] sm:w-[475px] w-[355px] ml-2 md:ml-[0px] sm:ml-[180px]  h-[55px]  flex justify-center items-center rounded-[10px] bg-secondary ' >
                    <h4 className='font-poppins text-white text-[15px] sm:text-[20px] md:text-[20px]'> <strong>20%</strong>  de remise sur la  <span className='font-semibold text-rose' >première publication</span></h4>
                </div>
                <div className='w-[590px] sm:w-full py-[50px]  px-0   m-[-10px]  sm:ml-[110px] md:ml-0     '>
                    <h1 className='font-poppins font-semibold ss:text[72px]  text-center md:text-start text-[30px] sm:text-[50px] w-[400px]  sm:w-full text-white sm:leading-[75px] leading-[40px] uppercase ' >première plateforme <br className='sm:block hidden'/>{" "}  d'appel d'offre <br className='sm:block hidden'/>{" "} du secteur privé</h1>
                </div>
                <div className='pt-[90px] ml-[0px] ss:flex md:flex sm:hidden  hidden  '>  
                    <Button/>
                </div>
            </div>
      
        
    </section>
  )
}

export default Header
