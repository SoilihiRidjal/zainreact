import React from 'react'
import styles from '../style'
import { arrowUp } from '../assets'

const GetStarted = () => (
    <div className={`${styles.flexCenter} w-[140px] h-[140px]
     rounded-full p-[2px] bg-blue-gradient cursor-pointer`}> 
      <div className={`${styles.flexCenter} p-[2px] flex-col bg-primary w-[100%] h-[100%] rounded-full`}>
          <div className={`${styles.flexStart} flex-row  `}>
            <p className=' font-poppins font-medium leading-[23px] text-[18px]'>
              <span className='text-gradient'>GET</span> 
            </p>
            <img src={arrowUp}  alt="arrow" className='w-[23px] h-[23px] object-contain' />
          </div>
          <p className=' font-poppins font-medium leading-[23px] text-[18px]'>
              <span className='text-gradient'>STARTED</span> 
          </p>
      </div>
      
    </div>
  )


export default GetStarted
