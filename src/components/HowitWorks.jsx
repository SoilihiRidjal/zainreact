import React from 'react'
import { attention } from '../assets'
import { howItWork } from '../constants'
import styles, { layout } from '../style'

const HowitWorks = () => {
  return (
    <section className={` ${styles.marginY} items-center justify-center`} >
        <h1 className='font-poppins font-semibold text-white text-[40px]' >Comment ça marche ?</h1>
        <div className=' w-full grid  grid-rows-1 grid-flow-dense  md:grid-rows-2 sm:grid-flow-col   sm:grid-rows-3 gap-12 mt-[45px] ' >
        {
            howItWork.map((work)=>(
                <div key={work.id} className = "flex">
                    <div className='w-[35px] h-[35px] rounded-full border-[5px] border-white flex items-center justify-center' >
                        <div className='w-[30px] h-[30px] rounded-full border-[5px] border-rose flex justify-center items-center ' >
                            
                        </div>
                    </div>
                    <p className={`${styles.paragraph}  max-w-[450px] mt-2 ml-8`} >
                    {work.tutorial}
                    </p>
                </div>
            ))
        }
        </div>
        <div className='w-full h-[250px] rounded-[20px] flex  items-center justify-center bg-secondary  sm:gap-5 mt-[45px] ' >
            <img src={attention} alt="" className='w-[90px] h-[70px] ' />
            <p className='font-poppins font-semibold text-white text-[15px] w-[900px] ' >Pour éviter les contrefaçon et le vol d'identité nos équipes vérifierons si votre entreprise existe avant de vous autoriser 
            toute publication  ou toute consultation</p>
        </div>
       
        
    </section>
  )
}

export default HowitWorks
