import React from 'react'
import styles from '../style'
import { robot,discount,manager } from '../assets'
import GetStarted from './GetStarted' 

const Hero = ()=>(
  <section id='Home' className={`flex md:flex-row flex-col ${styles.paddingY}`}>
    <div className={`flex-1 ${styles.flexStart} flex-col xl:px-0 sm:px-16 px-6`}>

      <div className="flex flex-row items-center py-[6px] px-4 bg-discount-gradient rounded-[16px] mb-2">
        <img src={discount} alt="discoutnt" className='w-[32px] h-[32px]'/>
        <p className={`${styles.paragraph} ml-2`}>
        <span className='text-white font-poppins font-semibold'>20%</span>
        {" "}de remise pour {" "}
        <span className='text-white font-poppins font-semibold'>La première annonce</span>
        </p>
      </div>

    <div className='flex flex-row w-full'>

      {/* div text */}
      <div className='py-3 flex flex-row'>
        <h1 className='flex-1 font-poppins font-semibold ss:text[72px] text-[52px]
         text-white ss:leading-[75px] leading-[75px]'>
          THE NEXT <br className='sm:block hidden'/>{" "}
          <span className='text-gradient' >
            Generation <br />
          </span>
        </h1>
      </div>
      {/* fin div text */}
      <div className=' ss:flex hidden md:mr-4 mr-0'> 
           <GetStarted/>
      </div>
     </div>
     <h1 className='text-white font-poppins font-semibold ss:text-[52px] text-[52px] 
     ss:leading-[60px] leading-[58px] w-full'>
          Paiment method
     </h1>
     <p className={`${styles.paragraph} max-w-[480px] mt-5`}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, placeat quas. Hic molestias voluptates sapiente, modi at eveniet culpa repellendus, fuga iure sed reprehenderit cumque consequuntur, quos error dolore eos.</p>
    </div>
    <div className={`flex-1 flex ${styles.flexCenter} md:my-0 my-10 relative`}>
      <img src={manager} alt="robot" className='w-[100%] h-[100%] relative z-[5]' />
      <div className=' absolute w-[40%] h-[35%] pink__gradient z-0 top-0'  />
      <div className=' absolute w-[80%] h-[80%] rounded-full z-1 bottom-40 white__gradient '  />
      <div className=' absolute w-[50%] h-[50%] blue__gradient z-0 bottom-20 right-20 '  />
    </div>
    <div className={` ss:hidden ${styles.flexCenter}`} >
        <GetStarted/>
    </div>
  </section>
)

export default Hero
