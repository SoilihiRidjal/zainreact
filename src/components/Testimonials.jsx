import React from 'react'
import styles,{layout} from '../style'
import FeedBack from './FeedBack'
import { feedback } from '../constants'
const Testimonials = () => (
  <section id='client' className={`${styles.paddingY} ${styles.flexCenter} flex-col relative`} >
      <div className='blue__gradient w-[60%] h-[60%] -right-[50%] rounded-full absolute z-[0] ' />
      <div className='flex flex-col justify-between items-center mb-6 relative z-[1] md:flex-row sm:mb16' > 
          <h1 className={`${styles.heading2} leading-[20px] `}  >Lorem ipsum dolor. </h1>
          <div className='w-full md:mt-0 mt-6' >
          <p className={`${styles.paragraph} text-left max-w-[450px]`} >Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptas quidem ut autem.</p>
          </div>
      </div>
      
      <div className=' flex flex-wrap sm:justify-start justify-center feedback-container relative w-full z-[1]'>
          {feedback.map((card)=>(
              <FeedBack key={card.id} {...card} />
          ))}
      </div>
  </section>
)

export default Testimonials
