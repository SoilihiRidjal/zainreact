import React, { useState } from 'react'
import { menu, monimo } from '../../assets'
import { LinkMenue } from '../../constants'
import { motion } from 'framer-motion'
import { close, } from '../../assets'
import { IoClose, IoMenu} from "react-icons/io5";
const Nav = () => {
  const [toggle, setToggle] = useState(false)
  const active = ()=>{
    setToggle(!toggle)
  }
  const variant = {
    ouver: { opacity: 1, x: 0 },
    closeder: { opacity: 0, x: "-100%" },

  }
  
  return (
    <>
    <nav className=' z-10 fixed top-0 bg-[#18133A] w-full h-[75] hidden md:flex flex-row items-center justify-between  ' >
     
        <div>
          <img src={monimo} alt="" className='w-[97px] h-[70px] ml-[76px] ' />
        </div>
        <div className='flex ml-[81px] mr-[41px] flex-1 items-center justify-between ' >
          <ul className=' w-9  list-none flex justify-center items-center flex-1' >
            {
              LinkMenue.map((nav,index)=>(
                <motion.li
                  key={nav.id}
                  whileHover={{ scale: 1.1 }}
                    className = {`text-white hover:text-lightpink  font-poppins font-medium text-[14px] ml-[40px]  transition-colors duration-700 `}
                > <a href={`#${nav.id}`}> {nav.title} </a> </motion.li>
              ))
            }
          </ul>
        </div>
        <div className= ' mr-11 w-[287px] h-[54px] flex justify-start items-center  ' >
          <motion.div whileHover={{scale:1.1}}  className=' w-[118px] h-[34px] rounded-[20px] ml-12px hover:bg-lightpink bg-lightblue flex items-center justify-center cursor-pointer transition-colors duration-1000 ' > 
            <h6 className='text-white font-poppins font-medium text-[14px]   ' >Connexion</h6> 
          </motion.div>
          <  motion.div whileHover={{scale:1.1}}  className=' ml-[26px] h-[34px]  ml-12px  block-gradient flex items-center justify-center  ' > 
            <h6 className='text-white font-poppins font-medium text-[14px] underline cursor-pointer  ' >Inscription gratuite </h6> 
          </motion.div>
        </div>
       
      
    </nav>
    <div className='  fixed  top-0 bg-[#18133A]  w-full md:hidden flex flex-1  items-center ' >
      <div className='m-[30px] w-[28px] h-[28px]  '  >
      <img 
            src={toggle ? menu : close} 
            alt="menue" 
            className='w-[28px] h-[28px] object-contain cursor-pointer'
            onClick={()=>active()}
            /> 
      </div>

      <motion.div className={`${toggle ? "hidden":"flex"} p-6 bg-[#18133A] md:flex absolute z-50 top-20 mx-4 my-6 min-w-[140px] sidebar  rounded-xl  `}
       
      >
        <ul className='list-none flex flex-col justify-start   flex-1'>
          {LinkMenue.map((nav,index)=>(
              <li
                key={nav.id}
                className = {`font-poppins font-normal cursor-pointer text-[13px] justify-start flex flex-row text-left
                              ${index === LinkMenue.length -  1 ? 'mr-0':'mb-4'} 
                            text-white `}
                >
                  <a href={`#${nav.id}`}>{nav.title}</a>
              </li>
          ))}
        </ul>
      </motion.div>
    </div>
   
   
   

    </>
  )
}

export default Nav
