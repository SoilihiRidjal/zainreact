import React from 'react'
import { cat } from '../../constants'
import styles, { layout } from '../../style'

const Categories = () => {
  return (
    <div className={`  bg-primary  `} >
        <h1 className=' w-[400px] sm:w-full mt-10  h-[56px] font-poppins font-bold gradient-text text-[20px] sm:text-[36px] px-6 sm:px-0 text-left sm:text-center ' >CONSULTER LES OFFRES <br className='flex sm:hidden' /> PAR CATEGORIES</h1>
        <div className=' grid grid-rows-1 sm:grid-cols-4 gap-y-[35px] sm:gap-y-10 gap-x-[48px] ml-[20px] sm:ml-[83px] mt-7' >
            
            {
                cat.map ((nav,index) => (
                    <div key={nav.id} className=' w-[256px] h-[159px] bg-[#1A1737] rounded-[20px]  ' >
                        <div  className={`${nav.classe} w-[152px] h-[33px] flex justify-center items-center rounded-[20px] mt-[13px] ml-[17px] `} ><h6 className='font-poppins w-[70px] h-[30px] text-[20px] gradient-text font-bold  ' >{nav.title}</h6></div>
                        <div className= ' w-full h-[30px] ' >
                                <h2 className=' font-poppins font-bold text-white text-[20px] ' > {nav.price}</h2>
                        </div>
                    </div>
                ))
            }
        </div>
            
      
    </div>
    
  )
}

export default Categories
