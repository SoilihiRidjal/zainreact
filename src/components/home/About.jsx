import React from 'react'
import { about } from '../../assets'

const About = () => {
  return (
    <div className=' w-[728px]  sm:w-full h-[1184px] pt-[0px] sm:pt-[49px]  sm:pl-[83px] bg-primary ' >
        <div className='flex items-center  w-full justify-evenly  flex-col sm:flex-row-reverse mt-11 ' >
            <div className='w-[507px] h-[407px] ml-[-160px] sm:ml-0 ' >
                <h1 className=' w-[300px] sm:w-[349px] h-[56px] font-poppins font-bold text-[20px]  sm:text-[36px] gradient-text ' > QUI SOMME NOUS </h1>
                <p className='w-[300px] sm:w-[464px] h-[328px] font-poppins font-light text-[13px]  sm:text-[16px] leading-[24px] text-white ' >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac accumsan ex. Quisque in ante a nibh rutrum suscipit. Vestibulum nec nisl massa. Duis quis tellus sed nunc tempus elementum a dapibus sapien. Suspendisse porta lacus sed egestas condimentum. Suspendisse dolor diam, aliquam id erat vitae, sodales tempus tellus. Vestibulum ipsum turpis, laoreet egestas augue quis, laoreet tincidunt eros. Donec consectetur eleifend elit at semper. Etiam imperdiet tortor nec tellus porta, ut tincidunt ante blandit. Phasellus tincidunt felis eu felis fringilla, fermentum sodales leo molestie. Sed dictum libero faucibus urna sollicitudin, in rutrum nibh auctor. Duis neque justo, venenatis id velit quis, semper interdum nisi.</p>
            </div>
            <img src={about} alt="" className='w-[320px] h-[200px] sm:w-[544px] sm:h-[311px] mt-[120px] ml-[-370px] sm:ml-1  ' />
        </div>

        <div className='flex items-center  w-full justify-center flex-col sm:flex-row mt-11 ' >
            <div className='w-[507px] h-[407px] ml-[-160px] ' >
                <h1 className=' w-[300px] sm:w-[349px] h-[56px] font-poppins font-bold text-[20px]  sm:text-[36px] gradient-text ' > POURQUOI NOUS CHOISIR </h1>
                <p className='w-[300px] sm:w-[464px] h-[328px] font-poppins font-light text-[13px]  sm:text-[16px] leading-[24px] text-white ' >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac accumsan ex. Quisque in ante a nibh rutrum suscipit. Vestibulum nec nisl massa. Duis quis tellus sed nunc tempus elementum a dapibus sapien. Suspendisse porta lacus sed egestas condimentum. Suspendisse dolor diam, aliquam id erat vitae, sodales tempus tellus. Vestibulum ipsum turpis, laoreet egestas augue quis, laoreet tincidunt eros. Donec consectetur eleifend elit at semper. Etiam imperdiet tortor nec tellus porta, ut tincidunt ante blandit. Phasellus tincidunt felis eu felis fringilla, fermentum sodales leo molestie. Sed dictum libero faucibus urna sollicitudin, in rutrum nibh auctor. Duis neque justo, venenatis id velit quis, semper interdum nisi.</p>
            </div>
            <img src={about} alt="" className='w-[320px] h-[200px] sm:w-[544px] sm:h-[311px] mt-[120px] ml-[-370px] sm:ml-32 ' />
        </div>
       
    </div>
  )
}

export default About
