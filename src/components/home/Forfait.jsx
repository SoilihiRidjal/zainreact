import React from 'react'
import { Price } from '../../constants'

const Forfait = () => {
  return (
    <section className=' sm:w-full w-[320px] ml-[25px]  mt-96 sm:mt-0 mr-10' > 
        
        <h1 className=' w-full h-[56px] text-[36px] forfait text-center font-poppins font-bold gradient-text' >Forfait</h1>

        <div className='  flex-col justify-center sm:ml-[-80px] sm:flex-row mt-2 sm:pl-[125px] grid grid-rows-1 gap-11 sm:grid-cols-3  sm:gap-9 pt-[20px] ' >
          {
            Price.map((nav)=>(
              <div className='w-[355px] h-[590px] bg-[#1A1737] flex flex-col items-center font-poppins ' key={nav.id} >
                      <div className=' w-[118px] h-[118px] rounded-full bg-[#171430] mt-3 flex justify-center items-center ' > <h3 className=' font-poppins font-medium gradient-text  text-[20px] ' > { nav.title } </h3> </div>
                      <h2 className=' w-[165px] h-[30px] text-[20px] text-white text-center ' > {nav.price} </h2>
                      <div className='flex flex-col justify-start' > 
                        <div className='flex mt-4 justify-start items-center gap-5 ' >
                          <div className=' w-[7px] h-[7px] rounded-full bg-[#8099E9] ' />
                          <h6 className=' text-white font-light' > Inscription : {nav.inscription} </h6>
                        </div>
                        <div className='flex mt-4 justify-start items-center gap-5 ' >
                          <div className=' w-[7px] h-[7px] rounded-full bg-[#8099E9] ' />
                          <h6 className=' text-white font-light  ' > Consultation : {nav.consultation} </h6>
                        </div>
                        <div className='flex mt-4 justify-start items-center gap-5 ' >
                          <div className=' w-[7px] h-[7px] rounded-full bg-[#8099E9] ' />
                          <h6 className=' text-white font-light ' > Publication : {nav.publication} </h6>
                        </div>
                        <div className='flex mt-4 justify-start items-center gap-5 ' >
                          <div className=' w-[7px] h-[7px] rounded-full bg-[#8099E9] ' />
                          <h6 className=' text-white font-light ' > Publicité : {nav.publicite} </h6>
                        </div>
                        <div className='flex mt-4 justify-start items-center gap-5 ' >
                          <div className=' w-[7px] h-[7px] rounded-full bg-[#8099E9] ' />
                          <h6 className=' text-white font-light ' > Postulation : {nav.postulation} </h6>
                        </div>
                        <div className='flex mt-4 justify-start items-center gap-5 ' >
                          <div className=' w-[7px] h-[7px] rounded-full bg-[#8099E9] ' />
                          <h6 className=' text-white font-normal ' > Alert : {nav.alert} </h6>
                        </div>
                      </div>
                      <div className={`${nav.button_gradient} mt-20 w-[120px] h-[40px] rounded-full flex justify-center items-center `} > <h6 className=' font-poppins font-medium text-white ' >Prendre</h6> </div>
              </div>
            ))
          }
            
        </div>
       
      
    </section>
  )
}

export default Forfait
