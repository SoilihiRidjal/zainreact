import React from 'react'
import Nav from './Nav'
import SectionHeader from './SectionHeader'
import styles from '../../style'
import Categories from './Categories'
import About from './About'
import Forfait from './Forfait'
import FooterHome from './FooterHome'
import Footer from '../Footer'


const Home = () => {
  return (
    <div className='bg-primary overflow-hidden' > 
        <div className={`${styles.boxWidth}`} >
          <Nav className="z-50" />
        </div>
        
        <div className={`bg-primary ${styles.flexStart}` }>
          <div className={`${styles.boxWidth}`}>
            <SectionHeader/>
          </div>
          
        </div>
        <Categories/>
        
        <About/> 
        <Forfait/>
        <Footer/>
        
    </div> 
  )
}

export default Home

