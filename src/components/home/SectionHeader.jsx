import React from 'react'
import { motion } from 'framer-motion'
import { bg_left } from '../../assets'

const SectionHeader = () => {
  return (
    <div className='flex flex-col md:flex-row  w-full my-16 ' >

        <div className='flex justify-start flex-col my-10 sm:my-0 ' >
       
            <div>
              <h1 className=' w-[350px] sm:leading-[65px] sm:w-[545px] h-[210px] font-poppins mx-3 sm:mx-[83px] my-[90px] text-[26px] sm:text-[50px] font-extrabold sm:text-start gradient-text  ' >Première plateforme d'appel d'offre aux comores</h1>
            </div>
            <div>
              <p className=' sm:mx-[83px] mx-3 my-[-160px] mb- sm:my-[-80px] w-[300px] sm:w-[457px] leading-[30px] h-[107px] text-[15px] sm:text-[20px] text-start font-poppins font-extralight text-white  ' >Nous créeons l’opportunité et nous fesons en sorte que nos utilisateurs puissent trouver  des offfre qui sont en concordance avec ces compétences</p>
            </div>
            <motion.div whileHover={{scale:1.1}}  className=' w-[118px] h-[34px] rounded-[20px]  button-gradient flex items-center justify-center cursor-pointer transition-colors duration-1000 mx-3 sm:mx-[83px]  sm:my-[52px] ' > 
              <h6 className='text-white font-poppins font-medium text-[14px]   ' >Tout voir</h6> 
            </motion.div>
        </div>
       <div className='w-[996px] h-[630px] hidden lg:flex ' >
          <img src={bg_left} alt="bg_left" className=' w-[996px] h-[630px]  ' />
       </div>
    </div>
  )
}

export default SectionHeader
