import React from 'react'
import styles from '../style'
import {HiPaperAirplane} from "react-icons/hi2"
const InputNewlestter = () =>(
    <form action="">
        <div className='relative flex items-center ' >
            <HiPaperAirplane size={20} color = "white" className=' absolute ml-[240px] sm:ml-[510px] md:ml-[190px]' />
            <input type="text" className=' sm:pr-[80px] md:pr-5 pr-12 pl-5 py-2 text-white w-full h-[60px] rounded-[20px] bg-secondary  ' placeholder='exemple@gmail.com'  />
        </div>
        {/* */}
    </form>
)

const NewLestter = () => {
  return (
    <div className={`${styles.marginY} ${styles.marginX} flex flex-col md:flex-row gap-10 mt-10 md:items-center md:justify-center  `} >
        <p className={`w-[320px] md:w-[400px] sm:w-full md:text-start sm:text-center  text-white font-poppins font-normal  `} >Abonnez-vous a notre  <span className= ' ml-1 font-poppins font-semibold text-rose text-[20px] sm:text-[30px] uppercase  mr-2 ' >newsletter</span> {"   "}
           et soyez plus prés des nouvelles offres 
        de la plateforme </p>
        <div>
            <InputNewlestter />
        </div>
        
    </div>
  )
}

export default NewLestter
