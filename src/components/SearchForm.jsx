import React from 'react'
import {HiOutlineMagnifyingGlass} from 'react-icons/hi2'

const SearchForm = () => {
  return (
    <div>
        <form action="">
            <div className='relative flex items-center'>
                <HiOutlineMagnifyingGlass size={20} color="white" className='absolute ml-3' />
                <input type="text" name='search' autoComplete='off' aria-label='recherch par categories'
                    className='pr-3 pl-10 py-2  text-white placeholder-gray-500  bg-transparent border-b-4 border-b-rose rounded-full '
                    placeholder='Recherche par catégories'
                />
         </div>
        </form>
        
        
    </div>
  )
}

export default SearchForm
