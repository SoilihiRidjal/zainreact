import {useState} from 'react'
import{close, logo, menu} from '../assets'
import { navLinks } from '../constants'
import SearchForm from './SearchForm'


const Navbar = () => {
    const [toggle,setToggle] = useState(false) 
  return (
   <nav className='w-full flex py-6 justify-between items-center navbar  ' >
        <h3 className='font-poppins font-semibold text-[22px] text-white  ' >Zain<span className='text-rose'>Market</span></h3>
        <div className=' ss:flex  hidden justify-between  px-[150px] flex-auto items-center ' >
            <p className=' md:flex hidden text-[12px] font-poppins font-normal text-white  ' >Moroni Zilmadjou (Bureau UCOMAD)</p>
            <p className=' md:flex hidden text-[12px] px-[45px] font-poppins font-normal text-white ' >323 44 50 / 456 67 98 / 773 45 17</p>
        </div>
        <div className='sm:flex hidden '>
            <SearchForm/>
        </div>
        <div>
            <img src="" alt="" />
        </div>

        <div className='sm:hidden flex flex-1 justify-end items-center'>
            <img 
            src={toggle ? close : menu} 
            alt="menue" 
            className='w-[28px] h-[28px] object-contain cursor-pointer'
            onClick={()=>setToggle(prev => !prev)}
            />

            <div className={` 
                            ${toggle ?'flex' : 'hidden'} 
                            p-6 bg-black-gradient absolute  
                            top-20 right-0 mx-4 my-2 min-w-[140px] rounded-xl sidebar`
                            }>
        <ul className='list-none flex flex-col justify-end items-center flex-1'>
                {navLinks.map((nav,index)=>(
                    <li
                        key={nav.id}
                        className = {`font-poppins font-normal cursor-pointer text-[13px]
                         ${index === navLinks.length -  1 ? 'mr-0':'mb-4'} 
                         text-white `}
                    >
                        <a href={`#${nav.id}`}>{nav.title}</a>
                    </li>
                ))}
        </ul>
            </div>
        </div>
   </nav>
  )
}

export default Navbar
