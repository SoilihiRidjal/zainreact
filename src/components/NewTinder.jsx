import React from 'react'
import { Link } from 'react-router-dom'
import { airbnb } from '../assets'
import { Tinder } from '../constants'
import styles, { layout } from '../style'
import Button from './Button'

const NewTinder = () => {
  return (
    <div>
        <div className='flex gap-5 justify-start items-center' >
            <h2 className={`font-poppins font-semibold text-white text-[35px]  `} >Nouvelle offre</h2>
            <h6 className='font-poppins mt-3 text-rose underline font-normal text-[20px]' >Tout voire</h6>
        </div>
            <section className={`${layout.section} mt-[-45px]  justify-center font-poppins text-white p-6 rounded-[20px]`}  >
       
                <div className=' flex flex-col' > 
                    {
                        Tinder.map((tinder)=>(
                            <div key={tinder.id} className=' w-[1100px] gap-[5] h-[200px] bg-secondary rounded-[10px] flex items-center  mb-6' >
                            <div className='flex items-center ml-[50px] justify-center w-[150px] '>
                                <div className='h-[108px] w-[108px] bg-transparent border-white border-[1px] rounded-full flex items-center justify-center  '>
                                    <img src={airbnb} alt=""  />
                                </div>
                            </div>
                            <div className=' justify-start ml-[40px] w-[500px] h-full' >
                                <h6 className='font-light mt-5' >{tinder.expiration}</h6>
                                <h1 className='font-bold text-[30px] mt-7 ' > {tinder.title.substring(0,25)+" . . ."} </h1>
                                <div className='flex justify-between mt-8 gap-10'>
                                    <h6 className='font-normal text-gray-300' > {tinder.ville} </h6>
                                    <h6 className='font-normal text-gray-300' > {tinder.categories} </h6>
                                    <h6 className='font-normal text-gray-300' > {tinder.type} </h6>
                                </div>
                            </div>
                            <div className='h-full w-[260px] ml-[100px] ' >
                                    <h6 className='font-normal text-gray-300 mt-5' > {tinder.publication} </h6>
                                    <div className='mt-3' >
                                        <Button />
                                    </div>
                                    
                                    <div className='flex items-center mt-7 gap-11' >
                                            <h6 className='font-normal text-gray-300' >Vue :{tinder.views} </h6>
                                            <h6 className='font-normal text-gray-300 ml-[10px]' > Reponse : {tinder.reponse} </h6>
                                    </div>
                            </div>
                        </div>
                        ))
                    }
                </div>
            </section>
    </div>

    
  )
}

export default NewTinder
