import React from 'react'
import styles, { layout } from '../style'
import { clients } from '../constants'
const Clients = () => (
  <section className={`${styles.flexCenter} my-4`} >
        
              <div className={`${styles.flexCenter} w-full flex-wrap`} >
                  {clients.map((element)=>
                    <div key={element.id} className={`${styles.flexCenter} flex-1 sm:min-w-[190px] min-w-[120px] `} >
                        <img src={element.logo} alt="" className='sm:w-[192px] w-[100px] object-contain' />
                    </div>
                  )}
              </div>
  
  </section>
)

export default Clients