import React from 'react'
import { card } from '../assets'
import styles, {layout} from '../style'
import Button from './Button'

const CardDeal = () => (
  <section className={layout.section}>
      <div className={`${layout.sectionInfo}`} >
          <h2 className={styles.heading2}>Lorem ipsum dolor <br className='sm:block hidden' /> sit amet consectetur.</h2>
          <p className={`${styles.paragraph} max-w-[480px] mt-5`} >Lorem, ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis et nesciunt excepturi veniam quidem cumque beatae ea corporis odit? Nobis blanditiis iure reiciendis necessitatibus libero, nam molestias corporis aliquid. Esse?</p>
          <Button styles={"mt-10"}/>
      </div>
      <div className={layout.sectionImg} >
        <img src={card} alt="" className='w-[100%] h-[100%] z-[6] relative ' />
      </div>
  </section>
)

export default CardDeal
