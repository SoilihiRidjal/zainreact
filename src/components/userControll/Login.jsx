import React from 'react'
import { arbre, women } from '../../assets'
import {HiBackspace, HiLockClosed, HiMapPin, HiOutlineArrowLeft, HiUser} from 'react-icons/hi2'
import { IoIosMail, IoLogoGoogle, IoLogoLinkedin } from "react-icons/io";
import { motion } from 'framer-motion'
import { useState } from 'react'
import axios from 'axios';
const Login = () => {
  //animation en design
    const variant = {
      open: { opacity: 1,
        x: 0,
        y:0,
        scale:1,
        transition:{
          duration:0.5
      }},
      closed: { opacity: 0,  },
      hidden : {opacity:0}

    }
    const [suscribe,UseSuscribe] = useState(false)
    const use = ()=>{
      UseSuscribe(!suscribe)
    }
    //animation en design

    //control form
    const [nameUser,setName] = useState('')
    const [mailUser,setMail] = useState('')
    const [adresse,setAdresse] = useState('')
    const [entreprise,setEntreprise] = useState(false)
    const [consultant,setCosultant] = useState(false)
    const [password,setPassword] = useState('')
    const [confirmPassword,setConfirmPassword] = useState('')

    const handleSubmit = (e)=>{
      e.preventDefault()
      const url = "http://localhost/tinder_app/"
      let fData = new FormData()
      fData.append('name_user',nameUser)
      fData.append('mail_user',mailUser) 
      fData.append('adresse',adresse)
      fData.append('checked_entreprise',entreprise)
      fData.append('checked_consultant',consultant)
      fData.append('password',password)
      axios.post(url,fData)
    }
    
    
  return (
  <div className='w-full h-[100vh]  bg-[#080430] flex items-center justify-center ' >
    <div className= ' font-poppins w-[1400px] h-[766px]   bg-black  flex items-center justify-center' >
            <div className=' bg-[url("./assets/women.jpg")] bg-cover  bg-no-repeat bg-center w-[766px] h-[766px] bg-lightblue hidden sm:block ' >
                {/* <img src={women} alt=""  /> */}
                {/* <h1 className='w-[535px] h-[120px] font-poppins font-extrabold text-[44px] text-center text-white uppercase m-[72px] ' > Bienvenue sur notre plateforme </h1>
                <p className='text-left w-[535px] h-[195px]  font-poppins font-thin text-white text-[20px] m-[72px] ' >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam venenatis vehicula leo. In ante leo, euismod imperdiet gravida eget, sollicitudin ac ante. Aenean venenatis, massa gravida pulvinar dignissim, tellus nibh condimentum erat, sed tempor mauris nulla at tortor. In hac habitasse platea dictumst. Proin eu ex eu nulla consectetur feugiat. Cras non mi vitae mauris euismod elementum sed non lorem. Proin vel lorem elementum augue ultricies vulputate. Cras laoreet ullamcorper nisl, nec pretium massa semper ut. Cras nunc ante, fringilla</p> */}
            </div> 

            <div className='flex flex-1 flex-row justify-center items-center bg-primary' >


            <motion.div className={suscribe ?'hidden' : 'w-[576px] h-[766px] flex flex-col p-[29px] bg-primary justify-items-center drop-shadow-md hover:drop-shadow-xl '} animate = {suscribe ? "closed" : "open"} variants={variant} >

      <form action="" className='flex flex-col items-center sm:w-full ' >
<h3 className=' text-lightblue sm:w-[433px] sm:h-[44px] w-full h-[30px] font-poppins font-medium text-center text-[25px] sm:text-[36px] mt-[74px]   ' > Veuillez vous identifier </h3>
          <div><h5 className='font-poppins font-thin text-white text-[20px] mt-4 ' >Continuer avec :</h5></div>
          <div className='w-[92px] h-[32px] mt-4 flex flex-row ' >
            <IoLogoGoogle size={30} className="text-white mr-7 cursor-pointer " />
            <IoLogoLinkedin size={30} className="text-white cursor-pointer " />
          </div>
          <div className='relative my-[30px] sm:my-[50px] w-full  ' >
              <input type="text" id='name' name='name_user' className='  bg-transparent w-[300px] sm:w-full px-[30px] ml-[110px] sm:ml-0 border-b-2 py-1  text-white border-b-white focus:outline-none ring-0  focus:border-b-2 transition-colors duration-700 peer/nom ' autoComplete='off' />
              <HiUser size={20}  className='absolute text-white left-0 my-[-27px] mx-4 ml-[110px] sm:ml-0 transition-colors duration-700 ' />
              <label htmlFor="nom" className=' absolute font-extralight text-[14px]  text-white ml-[140px] sm:ml-[30px]  left-0 top-2  cursor-text peer-focus/nom:text-xs peer-focus/nom:-top-3 transition-all duration-700    ' > Votre nom d'utilisateur </label>
          </div>
          <div className='relative w-full  ' >
              <input type="password" id='password' name='password' className='  bg-transparent w-[300px] sm:w-full ml-[110px] sm:ml-0 border-b-2 py-1 px-[30px] text-white border-b-white focus:outline-none ring-0  focus:border-b-2 transition-colors duration-700 peer/pass ' autoComplete='off' required />
              <HiLockClosed size={20}  className='absolute text-white left-0 my-[-27px] mx-4 ml-[110px] sm:ml-0' />
              <label htmlFor="password" className=' absolute font-extralight text-[14px]  text-white ml-[140px] sm:ml-[30px] left-0 top-2 cursor-text peer-focus/pass:text-xs peer-focus/pass:-top-3 transition-all duration-700' > Votre mot de passe </label>
          </div>

          <div className='relative my-[50px] flex items-center  w-[324px] h-[60px] bg-gray-200 ' >
            <input type="checkbox" id='checked' className=' w-[20px] h-[20px]   bg-transparent  ml-[20px]   text-white border-b-white focus:outline-none  focus:border-b-2 transition-colors duration-700 peer/nom ' autoComplete='off' />
            <label htmlFor="checked" className=' absolute font-extralight text-[14px] pr-[32px] text-black mx-12 left-0  transition-all duration-700' > Je ne suis pas un robot </label> 
          </div>

          <div className='relative  flex items-center justify-center  w-[324px] h-[60px]  ' >
            <motion.input type="submit" id='' className=' cursor-pointer w-[168px] h-[45px] bg-gray-gradient text-white  focus:outline-none  transition-colors duration-700 peer/nom ' value= "Connexion" 
             whileHover={{ scale: 1.1 }}
             onHoverStart={e => {}}
             onHoverEnd={e => {}}
          />
            <motion.h5 className='font-normal text-right text-[14px] text-[white] underline cursor-pointer ml-[20px]' onClick={()=>use()} whileHover={{ scale: 1.1 }}
             onHoverStart={e => {}}
             onHoverEnd={e => {}}  >s'inscrire</motion.h5>
          </div>
      </form>
            </motion.div>
  
      {
          suscribe ? (<>
            < motion.div  className=' w-[576px] h-[766px] flex flex-col p-[29px] bg-primary justify-items-center drop-shadow-md hover:drop-shadow-xl ' 
                          initial={{ opacity: 0, y: -40 }}
                          animate={{ opacity: 1, }}
                          transition={{ duration: 0.5 }}
                          >
              <div className='flex flex-row items-center   mt-[90px] ' >
                <HiOutlineArrowLeft size={20} className = "text-white cursor-pointer ml-[100px] sm:ml-0" onClick={()=>use()}  />
                <h3 className=' text-lightblue font-poppins font-medium  text-[25px] sm:text-[36px] ml-[40px]  ' > Inscrivez-vous </h3>
              </div>
              
                   <form onSubmit={handleSubmit} className="flex flex-col items-center justify-center " >
                        
                        <div className='relative my-[40px] ' >
                          <input type="text" id='name' name='name_user'  className='  bg-transparent w-[300px] sm:w-[490px] border-b-2 px-[30px] text-white  border-b-white focus:outline-none ring-0 placeholder-white font-extralight  focus:border-b-2 transition-colors duration-700 peer/nom ' autoComplete='off'  placeholder = "Votre nom d'utilisateur" value={nameUser} onChange={(e)=>setName(e.target.value)} />
                          <HiUser size={20}  className='absolute text-white left-0 my-[-27px] ' />
                          {/* <label htmlFor="name" className={setInputs ? null : 'absolute font-extralight text-[14px]  text-white mx-12 left-0 top-1 cursor-text peer-focus/nom:text-xs peer-focus/nom:-top-3 transition-all duration-700 '} > Votre nom d'utilisateur </label> */}
                        </div>
                        <div className='relative mt-[-20px] ' >
                          <input type="text" id='adresse' name='adresse' className='  bg-transparent w-[300px] sm:w-[490px] border-b-2 py-1 px-[30px] text-white placeholder-white border-b-white focus:outline-none ring-0  focus:border-b-2 transition-colors duration-700 peer/nom font-extralight'  placeholder='Votre adresse' autoComplete='off' value={adresse} onChange={(e)=>setAdresse(e.target.value)} />
                          <HiMapPin size={20}  className='absolute text-white left-0 my-[-27px] ' />
                          {/* <label htmlFor="adresse" className=' absolute font-extralight text-[14px]  text-white mx-12 left-0 top-1 cursor-text peer-focus/nom:text-xs peer-focus/nom:-top-3 transition-all duration-700    ' > Votre adresse </label> */}
                        </div>
                        <div className='relative  mt-[20px]' >
                           <input type="text" id='mail_suscribe' name='mail_user' className='w-[300px] sm:w-[490px]   px-[30px] py-1 text-white placeholder-white border-b-2 border-b-white bg-transparent focus:outline-none peer/suscribe font-extralight  ' placeholder='Votre e-mail' value={mailUser} onChange={(e)=>setMail(e.target.value)}  />
                           <IoIosMail size={20}  className='absolute text-white left-0 my-[-27px] ' />
                           {/* <label htmlFor="mail_suscribe" className=' absolute left-0  mx-12 text-[14px] top-1 cursor-text  text-white font-poppins font-normal peer-focus:/suscribe:text-xs peer-focus/suscribe:-top-3 transition-all duration-700 ' >Votre mail</label> */}
                       </div>
                       <div className=' my-[40px] w-[300px] sm:w-[411px]  ' >
                          <h6 className='font-semibold text-white text-[14px]  ' >Rôle utilisateur : </h6>
                          <div className='flex flex-1 flex-row items-center justify-between' >
                            <div className='flex' >
                                <input type="checkbox" id='checked_entreprise'  name='checked_entreprise' className=' w-[20px] h-[20px]   bg-transparent    text-white border-b-white focus:outline-none  focus:border-b-2 transition-colors duration-700 peer/nom font-extralight ' autoComplete='off' value={entreprise} onChange={(e)=>setEntreprise(e.target.value)}  />
                                <label htmlFor="checked_entreprise" className='  font-extralight text-[14px] pr-[32px] text-white mx-4  transition-all duration-700' > Entreprise </label>
                              </div>
                              <div className=' flex' >
                                <input type="checkbox" id='checked_consultant'  name='checked_consultant' className=' w-[20px] h-[20px]   bg-transparent  ml-[20px]   text-white border-b-white focus:outline-none  focus:border-b-2 transition-colors duration-700 peer/nom  font-extralight' autoComplete='off' value={consultant} onChange={(e)=>setCosultant(e.target.value)}  /> 
                                <label htmlFor="checked_consultant" className='  font-extralight text-[14px] pr-[32px] text-white mx-4   transition-all duration-700' > Consultant</label>
                              </div> 
                            </div>
                        </div>
                        <div className='relative ' >
                          <input type="password" id='password_suscribe' name='password' className='  bg-transparent w-[300px]  sm:w-[490px] placeholder-white border-b-2 py-1 px-[30px] text-white border-b-white focus:outline-none ring-0  focus:border-b-2 transition-colors duration-700 peer/nom font-extralight ' autoComplete='off' placeholder='Mot de passe' value={password} onChange={(e)=>setPassword(e.target.value)}  />
                          <HiLockClosed size={20}  className='absolute text-white left-0 my-[-27px] ' />
                          {/* <label htmlFor="password_suscribe" className=' absolute font-extralight text-[14px]  text-white mx-12 left-0 top-1 cursor-text peer-focus/nom:text-xs peer-focus/nom:-top-3 transition-all duration-700    ' > Votre mot de passe </label> */}
                        </div>
                        <div className='relative my-[20px] ' >
                          <input type="password" id='confirm_password' name='confirm_password' className='  bg-transparent w-[300px] sm:w-[490px] border-b-2 py-1 px-[30px] text-white border-b-white focus:outline-none ring-0  focus:border-b-2 transition-colors duration-700  placeholder-white placeholder-thin font-extralight  peer/nom ' autoComplete='off' placeholder='Confirmer votre mot de passe' value={confirmPassword} onChange={(e)=>setConfirmPassword(e.target.value)} />
                          <HiLockClosed size={20}  className='absolute text-white left-0 my-[-27px] ' />
                          {/* <label htmlFor="password" className=' absolute font-extralight text-[14px]  text-white mx-12 left-0 top-1 cursor-text peer-focus/nom:text-xs peer-focus/nom:-top-3 transition-all duration-700    ' > Confirmer votre mot de passe </label> */}
                        </div>
                        <div className='relative my-[50px] flex items-center ml-32 w-[324px] h-[60px]  ' >
                          <motion.input type="submit" id='' className=' cursor-pointer w-[168px] h-[45px] bg-gray-gradient text-white  focus:outline-none  transition-colors duration-700 peer/nom ' value= "Inscription" 
                            whileHover={{ scale: 1.1 }}
                            onHoverStart={e => {}}
                            onHoverEnd={e => {}}
                          />
                        </div>
                   </form>
             </motion.div>
         </>) :null
      }
            </div>
        
         
         
    </div>
  </div>
  
  )
}

export default Login
