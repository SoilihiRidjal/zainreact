import React from 'react'
import { icon } from '../../../constants'
import { IoAccessibility } from 'react-icons/io5'
import { IoIosList } from 'react-icons/io'

const SideBar = () => {
  return (
    <div className=' flex flex-col items-center  bg-primary  h-[100vh] w-[75px]' >
        <div className=' w-[41px] h-[41px] rounded-full bg-slate-400 mt-[24px] ' >
           
        </div>
        <div className='w-[75px] flex justify-end items-end mt-10 ' >
            <div className=' w-[66px] h-[440px] nv rounded-l-[40px] flex flex-col items-center   ' >
                {
                    icon.map ((nav,index)=>(
                        <div className={`w-[41px] h-[41px] mt-9  rounded-full ${nav.gradient} flex justify-center items-center `} >
                            <div className='w-[35px] h-[35px] bg-primary rounded-full flex justify-center items-center ' >
                                
                            </div>
                        </div>
                    ))
                }
                
            </div>
        </div>
         
    </div>
  )
}

export default SideBar
