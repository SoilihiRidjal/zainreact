import { useRive } from '@rive-app/react-canvas'
import React from 'react'

const Simple = () => {
    const { rive, RiveComponent } = useRive({
        src: 'tête_de_mort.riv',
        autoplay: true,
      });
  return (
        <RiveComponent/>
  )
}

export default Simple